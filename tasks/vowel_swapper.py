def vowel_swapper(string):


    # ==============
    # Your code here
    inp = string
    change = {'A': '4', 'a': '4',
          'E': '3', 'e': '3',
          'I': '!', 'i': '!',
          'O': 'ooo', 'o': 'ooo',
          'U': '|_|', 'u': '|_|'}
    
    for char in inp:

        print(change.get(char, char), end='')
    
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console